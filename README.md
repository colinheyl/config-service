# README #
### Purpose ###
Built as part of SPAR-99 (https://umbria.atlassian.net/browse/SPAR-99).
This is an investigation into having a centralised configuration service so that our apps don't require new releases just to get new config.

This is based on the Spring guide: https://spring.io/guides/gs/centralized-configuration/

### How do I get set up? ###
To Use:
- Run the configuration service (/home/colin/IdeaProjects/config-service/complete/configuration-service/src/main/java/com/example/configurationservice/ConfigurationServiceApplication.java)
- Go to: http://localhost:8888/actuator/refresh
- Then run the configuration client(/home/colin/IdeaProjects/config-service/complete/configuration-client/src/main/java/com/example/configurationclient/ConfigurationClientApplication.java)
- Then go to: http://localhost:8080/message
    - If it fails, the message endpoint should show "Default Message"
    - If it works as intended, then you should see a different message, which is currently "This is Sparta!"   
- If you change the config in the remote repo and do a refresh on the service, you will need to restart the client to pull in the new value
   
### How does it work?###
- There is a git repo at https://bitbucket.org/colinheyl/sparta-config/src/master/ which holds a .properties file
- The client's "spring.application.name" value matches the name of the .properties file in the remote git repo.
- Hitting the refresh endpoint of the service will pull the latest config from the repo
- When started, the config for the client will be replaced with what is in that remote .properties file, which in this case just contains a message
- When you hit the /message endpoint of the client, you will see the message